// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Obstacles.generated.h"

UCLASS()
class ENDLESSRUNNERCPP_API AObstacles : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties


protected:
	AObstacles();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* ObstacleMesh;

	UFUNCTION(BlueprintImplementableEvent)
		void OnTrigger(class ARunCharacter* runCharacter);

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


};
