// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, class ARunCharacter*, runCharacter);

UCLASS()
class ENDLESSRUNNERCPP_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	ARunCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UCameraComponent* Camera;

	UFUNCTION(BlueprintCallable)
		void Die();

	UPROPERTY(BlueprintAssignable)
		FDeathSignature OnDeath;

	UFUNCTION(BlueprintCallable)
		void AddCoin(int32 numberToAdd);




public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool isDead;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coins")
		int32 Coins;

};
