// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "HAL/Platform.h"
#include "TimerManager.h"

ARunGameMode::ARunGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}
void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Start!"));
	for (int i = 0; i < 5; i++)
	{
		AddFloorTile();
	}
}

void ARunGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARunGameMode::AddFloorTile()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	int32 randomInteger = FMath::RandRange(0, TileArray.Num() - 1);
	
	ATile* tile = GetWorld()->SpawnActor<ATile>(TileArray[randomInteger], nextSpawnPoint, spawnParams);

	nextSpawnPoint = tile->GetAttachedTransform();

	tile->OnTileExit.AddDynamic(this, &ARunGameMode::OnTileExited);
}

void ARunGameMode::OnTileExited(ATile* tile)
{
	AddFloorTile();
	GetWorldTimerManager().SetTimer(timerHandle, FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTiles, tile), 1.0f, false);
}

void ARunGameMode::DestroyTiles(ATile* tileToDestroy)
{
	tileToDestroy->Destroy();
}
