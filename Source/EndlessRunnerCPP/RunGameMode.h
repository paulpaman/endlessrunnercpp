// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNERCPP_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	ARunGameMode();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Tiles")
		TArray<TSubclassOf<class ATile>> TileArray;

	UFUNCTION()
		void AddFloorTile();

	UFUNCTION()
		void OnTileExited(class ATile* tile);

	UFUNCTION()
		void DestroyTiles(class ATile* tileToDestroy);

	
	

private:
	FTimerHandle timerHandle;
	FTransform nextSpawnPoint;
};
