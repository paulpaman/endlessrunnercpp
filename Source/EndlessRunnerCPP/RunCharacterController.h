// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNERCPP_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

protected:
	ARunCharacterController();
	virtual void BeginPlay() override;

	void MoveForward(float scale);
	void MoveRight(float scale);

	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	class ARunCharacter* runCharacter;
	
public: 
	UFUNCTION()
		void DisableControlllerInput();
};
