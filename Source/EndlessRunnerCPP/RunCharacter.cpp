// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "RunCharacterController.h"
#include "GameFramework/Pawn.h"
#include "GenericPlatform/GenericPlatform.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	//SpringArm->SetRelativeLocation(FVector(-500.0f, 0, 0));
	SpringArm->SocketOffset = FVector(-200.0f, 0, 100.0f);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	isDead = false;
}

void ARunCharacter::Die()
{
	if (isDead == true)
	{
		Cast<ARunCharacterController>(GetController())->DisableControlllerInput();
		GetMesh()->SetVisibility(false);
		OnDeath.Broadcast(this);
	}
}

void ARunCharacter::AddCoin(int32 numberToAdd)
{
	Coins += numberToAdd;
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

