// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"
#include "Kismet/KismetInputLibrary.h"

ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	runCharacter = Cast<ARunCharacter>(GetPawn());
}

void ARunCharacterController::MoveForward(float scale)
{
	runCharacter->AddMovementInput(runCharacter->GetActorForwardVector() * scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	runCharacter->AddMovementInput(runCharacter->GetActorRightVector() * scale);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (runCharacter->isDead == false)
	{
		MoveForward(1);
	}
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//InputComponent->BindAxis("MoveForward", this, &ARunCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

void ARunCharacterController::DisableControlllerInput()
{
	DisableInput(Cast<ARunCharacterController>(this));
}

