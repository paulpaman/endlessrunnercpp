// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Math/TransformNonVectorized.h"
#include "RunCharacter.h"
#include "Obstacles.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/EngineTypes.h"
#include "Engine/World.h"
#include "Tile.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(SceneComponent);

	SpawnTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnTrigger"));
	SpawnTrigger->SetupAttachment(SceneComponent);

	PickupTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupTrigger"));
	PickupTrigger->SetupAttachment(SceneComponent);
}

FTransform ATile::GetAttachedTransform()
{
	FTransform attachTransform = AttachPoint->GetComponentTransform();

	return attachTransform;
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnOverlapBegin);

	SpawnPickupsAndObstacles();
}

void ATile::Destroyed()
{
	Super::Destroyed();

	for (AObstacles* obstacles : ObstacleSpawns)
	{
		obstacles->Destroy();
	}

	for (APickup* pickups : PickupSpawns)
	{
		pickups->Destroy();
	}
}

void ATile::OnOverlapBegin(UPrimitiveComponent* newComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(OtherActor))
	{
		OnTileExit.Broadcast(this);
	}
}

FVector ATile::GetRandomPointInBoundingBox(UBoxComponent* BoundingBox)
{
	FVector Origin = BoundingBox->Bounds.Origin;
	FVector BoxExtent = BoundingBox->GetScaledBoxExtent();

	return UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtent);
}

FTransform ATile::GetSpawnTransform(UBoxComponent* SpawnBox)
{
	FVector Location = GetRandomPointInBoundingBox(SpawnBox);
	FRotator Rotation = GetRandomPointInBoundingBox(SpawnBox).Rotation();
	FVector Scale = FVector(1.0f);

	return UKismetMathLibrary::MakeTransform(Location, Rotation, Scale);
}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	int32 randomInteger = FMath::RandRange(0, ObstacleArray.Num() - 1);
	AObstacles* spawnedObstacle = GetWorld()->SpawnActor<AObstacles>(ObstacleArray[randomInteger], GetSpawnTransform(SpawnTrigger), spawnParams);
	spawnedObstacle->AttachToComponent(SpawnTrigger, FAttachmentTransformRules::KeepWorldTransform);
	ObstacleSpawns.Add(spawnedObstacle);
}

void ATile::SpawnCoin()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	int32 randomInteger = FMath::RandRange(0, PickupArray.Num() - 1);
	APickup* spawnedPickup = GetWorld()->SpawnActor<APickup>(PickupArray[randomInteger], GetSpawnTransform(PickupTrigger), spawnParams);
	spawnedPickup->AttachToComponent(PickupTrigger, FAttachmentTransformRules::KeepWorldTransform);
	spawnedPickup->SetActorRelativeRotation(FRotator(-90.0f, 0, 0));
	PickupSpawns.Add(spawnedPickup);
}

bool ATile::RandomBoolWithWeight(float percentChanceConfig)
{
	float percentChance = percentChanceConfig / 100.0f;

	float randomFloat = FMath::RandRange(0, 1);

	if (percentChance <= randomFloat)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ATile::SpawnPickupsAndObstacles()
{
	bool canSpawnObstacle;
	bool canSpawnPickup;
	
	for (int i = 0; i < MaxSpawnOfObstacle - 1; i++)
	{
		canSpawnObstacle = RandomBoolWithWeight(percentageOfObstacleSpawning);
		if (canSpawnObstacle == true)
		{
			SpawnObstacle();
		}
	}
	
	for (int i = 0; i < MaxSpawnOfPickup - 1; i++)
	{
		canSpawnPickup = RandomBoolWithWeight(percentageOfPickupSpawning);
		if (canSpawnPickup == true)
		{
			SpawnCoin();
		}
	}
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

