// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTargetCollidedSignature, class ATile*, Tile);

UCLASS()
class ENDLESSRUNNERCPP_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintAssignable)
		FTargetCollidedSignature OnTileExit;

	UFUNCTION()
		FTransform GetAttachedTransform();

protected:
	ATile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* PickupTrigger;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* newComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* SpawnTrigger;

	UFUNCTION()
		FVector GetRandomPointInBoundingBox(UBoxComponent* BoundingBox);

	UFUNCTION()
		FTransform GetSpawnTransform(UBoxComponent* SpawnBox);

	UPROPERTY(EditAnywhere, Category = "Obstacle Array")
		TArray<TSubclassOf<class AObstacles>> ObstacleArray;

	UPROPERTY(EditAnywhere, Category = "Pickup Array")
		TArray<TSubclassOf<class APickup>> PickupArray;

	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
		void SpawnCoin();

	UPROPERTY(EditAnywhere, Category = "Max Spawn Amount")
		int32 MaxSpawnOfObstacle;

	UPROPERTY(EditAnywhere, Category = "Max Spawn Amount")
		int32 MaxSpawnOfPickup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PercentChanceConfig")
		float percentageOfPickupSpawning;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PercentChanceConfig")
		float percentageOfObstacleSpawning;
	
	UFUNCTION()
		bool RandomBoolWithWeight(float percentChanceConfig);

	UFUNCTION()
		void SpawnPickupsAndObstacles();

	TArray<class AObstacles*> ObstacleSpawns;
	TArray<class APickup*> PickupSpawns;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};
